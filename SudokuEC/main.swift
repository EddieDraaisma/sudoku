//
//  main.swift
//  SudokuEC
//
//  Created by Eddie Draaisma on 25/01/2019.
//  Copyright © 2019 Draaisma. All rights reserved.
//

import Foundation


struct colConstraint: Hashable {

    let text: String
    let value1: Int
    let value2: Int
    
    init(_ t: String, _ v1: Int, _ v2: Int) { text = t; value1 = v1; value2 = v2 }
}


struct rowSolution: Hashable {
    
    let row: Int
    let col: Int
    let value: Int
    init(_ r: Int, _ c: Int, _ v: Int) { row = r; col = c; value = v }
}


func generateBoardFromString(_ gameStr: String) -> [Int] {
    
    var board = Array(repeating: 0, count: 81)
    var pos = 0
    for chr in gameStr {
        board[pos] = Int(String(chr)) ?? 0
        pos += 1
    }
    return board
}


func solveBoard(_ board: [Int]) -> [Int] {
    var board = board
    

    var Y: [rowSolution: [colConstraint]] = [:]
    var X: [colConstraint: Set<rowSolution>] = [:]
    
    for row in 0 ..< 9 {
        for col in 0 ..< 9 {
            for num in 1 ... 9 {
                let box = (row / 3) * 3 + col / 3
                let celConstraints = [colConstraint("rc", row, col),
                                   colConstraint("rn", row, num),
                                   colConstraint("cn", col, num),
                                   colConstraint("bn", box, num)]
                Y[rowSolution(row, col, num)] = celConstraints
                for constraint in celConstraints {
                    X[constraint, default: []].insert(rowSolution(row, col, num))
                }
            }
        }
    }
    for pos in 0 ..< 81 {
        if board[pos] != 0 {
            let _ = select(&X, Y, rowSolution(pos / 9, pos % 9, board[pos]))
        }
    }
    let solution = solve(X, Y)
    for row in solution {
        board[row.row * 9 + row.col] = row.value
    }
    return board
}


func printBoard(_ board: [Int]) {
    print()
    for row in 0 ..< 9 {
        if [3, 6].contains(row) {
            print(" ------+-------+------")
        }
        print(" ", terminator: "")
        for col in 0 ..< 9 {
            if [3, 6].contains(col) {
                print("| ", terminator: "")
            }
            print(board[row * 9 + col], terminator: " ")
        }
        print()
    }
    print()
}


let grid1 = "003020600900305001001806400008102900700000008006708200002609500800203009005010300"
let hard1 = ".....6....59.....82....8....45........3........6..3.54...325..6.................."
let most  = "061007003092003000000000000008530000000000504500008000040000001000160800600000000"
let grid2 = "4.....8.5.3..........7......2.....6.....8.4......1.......6.3.7.5..2.....1.4......"
let difficult = "800000000003600000070090200050007000000045700000100030001000068008500010090000400"
let diff = "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
let diff2 = "980700000600090800005008090500006080000400300000070002090300700050000010001020004"
let xx = "...8.1..........435............7.8........1...2..3....6......75..34........2..6.."
let snyder = " 5  2  3 2    17 84 76          5   52     47   7          35 43 65    1 9  7  6 "


let gameStr = xx



printTimeElapsedWhenRunningCode {
    printBoard(solveBoard(generateBoardFromString(gameStr)))
}
