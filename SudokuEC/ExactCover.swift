//
//  ExactCover.swift
//  SudokuEC
//
//  Created by Eddie Draaisma on 27/01/2019.
//  Copyright © 2019 Draaisma. All rights reserved.
//

import Foundation


func select<T, Z>(_ X: inout [Z: Set<T>], _ Y: [T: [Z]], _ row: T) -> [Set<T>] {
    
    var cols: [Set<T>] = []
    for constraint1 in Y[row]! {
        for cell in X[constraint1]! {
            for constraint2 in Y[cell]! {
                if constraint1 != constraint2 {
                    X[constraint2]!.remove(cell)
                }
            }
        }
        cols.append(X.removeValue(forKey: constraint1)!)
    }
    return cols
}


func deselect<T, Z>(_ X: inout [Z: Set<T>], _ Y: [T: [Z]], _ row: T, _ cols: inout [Set<T>]) {
    for constraint1 in Y[row]!.reversed() {
        X[constraint1] = cols.popLast()
        for cell in X[constraint1]! {
            for constraint2 in Y[cell]! {
                if constraint2 != constraint1 {
                    X[constraint2]!.insert(cell)
                }
            }
        }
    }
}


func solve<T, Z>(_ X: [Z: Set<T>], _ Y: [T: [Z]]) -> [T] {

    var solution: [T] = []
    var X = X
    
    func solveLocal(_ X: inout [Z: Set<T>], _ Y: [T: [Z]]) -> [T]? {
        
        if X.isEmpty {
            return solution
        }
        else {
            let c = X.min(by: {$0.value.count < $1.value.count})!
            for row in c.value {
                solution.append(row)
                var cols = select(&X, Y, row)
                if let s = solveLocal(&X, Y) {
                    return s
                }
                deselect(&X, Y, row, &cols)
                _ = solution.popLast()
            }
        }
        return nil
    }
    return solveLocal(&X, Y) ?? []
}
